
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h3 class="login-box-msg">Silahkan Login</h3>
         <?= $this->session->flashdata('notif'); ?>
      <form action="" method="post">
         <?= form_error('email', '<small class="text-danger pl-3">', '</small>');  ?>
        <div class="input-group mb-3">
          <input type="text" class="form-control" value="<?= set_value('email') ?>" autocomplete="off" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
         <?= form_error('password', '<small class="text-danger pl-3">', '</small>');  ?>
        <div class="input-group mb-3">
          <input type="password" value="<?= set_value('email') ?>" autocomplete="off" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-0">
        <a href="<?= base_url('auth/register'); ?>" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->


