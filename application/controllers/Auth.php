<?php  

 class Auth extends CI_Controller
 {
 	
 	function __construct() 
 	{
 		parent::__construct();
		$this->load->model('Auth_Model');
		$this->load->library('form_validation');
 	}
 
 	public function index()
 	{

 		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

 		if ($this->form_validation->run() == FALSE) {
	 		$data['judul']= 'Halaman Login';
	 		$this->load->view('tempauth/header', $data);
	 		$this->load->view('Auth/login');
	 		$this->load->view('tempauth/footer');
 		} else{
 			$this->Auth_Model->login();
 		}
 		
 	}


 	public function register()
 	{
 		$this->form_validation->set_rules('nama', 'Name', 'required|trim'); //trim untuk mencegah spasi masuk ke db
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[7]|matches[password2]',
		[
			'matches' => 'Konfirmasi password tidak sama!',
			'min_length' => 'Password minimal 7 angka!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

 		if ($this->form_validation->run() == FALSE) {
	 		$data['judul']= 'Halaman Register';
	 		$this->load->view('tempauth/header', $data);
	 		$this->load->view('Auth/register');
	 		$this->load->view('tempauth/footer');
 		} else{
 			$this->Auth_Model->register();
 			$this->session->set_flashdata('notif', 
			'<div class="alert alert-success" role="alert">
			Selamat!, Akun baru berhasil dibuat. Silahkan Masuk!
			</div>');
			redirect('/auth');
 		}

 	
 	}

 	public function keluar()
 	{
 		$this->session->unset_userdata('email');
 		$this->session->unset_userdata('role_id');

		$this->session->set_flashdata('notif', 
		'<div class="alert alert-success" role="alert">
		Kamu Berhasil keluar!
		</div>');
		redirect('/auth');

 	}


 }

?>