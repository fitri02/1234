<?php  

class Auth_Model extends CI_Model
{
	
	public function register() {
		
		$data = [
		'nama'=> htmlspecialchars($this->input->post('nama', true)),
		'email'=> htmlspecialchars($this->input->post('email', true)),
		'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
		'role_id' =>3,
	];
		$this->db->insert('tb_user', $data);
	}

	public function login() {

	$email = $this->input->post('email');
	$password = $this->input->post('password');

	$user= $this->db->get_where('tb_user', ['email'=> $email])->row_array();

	if ($user) {
			//cek password
			if (password_verify($password, $user['password'])) {
				
				$data= [
					'email' => $user['email'],
					'role_id' => $user['role_id']
				];

				$this->session->userdata($data);
				if ($user['role_id'] == 1) {
					redirect('superadmin');
				} elseif ($user['role_id'] == 2) {
					redirect('admin');
				} else{
					redirect('user');
				}
				

			} else{
				$this->session->set_flashdata('notif', 
				'<div class="alert alert-danger" role="alert">
					password anda salah!
				</div>');
				redirect('auth');
			}

		} else{
		$this->session->set_flashdata('notif', 
			'<div class="alert alert-danger" role="alert">
				Email tidak terdaftar!
			</div>');
		redirect('auth');
		}

}


	
}

?>